﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
  public interface IDrawerService
    {
        IEnumerable<Drawer> GetAll();
        Drawer GetById(Guid id);
        int Insert(Drawer entity);
        int Update(Drawer entity);
        int Delete(Guid id);
        int Delete(Drawer entity);
        int Delete(Expression<Func<Drawer, bool>> where);
        Drawer Get(Expression<Func<Drawer, bool>> where);
        IEnumerable<Drawer> GetMany(Expression<Func<Drawer, bool>> where);
    }
}
