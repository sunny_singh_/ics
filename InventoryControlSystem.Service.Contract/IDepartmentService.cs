﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IDepartmentService
    {
        IEnumerable<Department> GetAll();
        Department GetById(Guid id);
        int Insert(Department entity);
        int Update(Department entity);
        int Delete(Guid id);
        int Delete(Department entity);
        int Delete(Expression<Func<Department, bool>> where);
        Department Get(Expression<Func<Department, bool>> where);
        IEnumerable<Department> GetMany(Expression<Func<Department, bool>> where);
    }
}
