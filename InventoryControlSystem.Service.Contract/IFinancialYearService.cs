﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    
   public interface IFinancialYearService
    {
        IEnumerable<FinancialYear> GetAll();
        FinancialYear GetById(Guid id);
        int Insert(FinancialYear entity);
        int Update(FinancialYear entity);
        int Delete(Guid id);
        int Delete(FinancialYear entity);
        int Delete(Expression<Func<FinancialYear, bool>> where);
        FinancialYear Get(Expression<Func<FinancialYear, bool>> where);
        IEnumerable<FinancialYear> GetMany(Expression<Func<FinancialYear, bool>> where);
    }
}
