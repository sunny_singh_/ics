﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
  public  interface ILocationService
    {
        IEnumerable<Location> GetAll();
        Location GetById(Guid id);
        int Insert(Location entity);
        int Update(Location entity);
        int Delete(Guid id);
        int Delete(Location entity);
        int Delete(Expression<Func<Location, bool>> where);
        Location Get(Expression<Func<Location, bool>> where);
        IEnumerable<Location> GetMany(Expression<Func<Location, bool>> where);
    }
}
