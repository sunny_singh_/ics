﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IStockService
    {
        IEnumerable<Stock> GetAll();
        Stock GetById(Guid id);
        int Insert(Stock entity);
        int Update(Stock entity);
        int Delete(Guid id);
        int Delete(Stock entity);
        int Delete(Expression<Func<Stock, bool>> where);
        Stock Get(Expression<Func<Stock, bool>> where);
        IEnumerable<Stock> GetMany(Expression<Func<Stock, bool>> where);
    }
}
