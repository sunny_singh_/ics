﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IProductService
    {
        IEnumerable<Product> GetAll();
        Product GetById(Guid id);
        int Insert(Product entity);
        int Update(Product entity);
        int Delete(Guid id);
        int Delete(Product entity);
        int Delete(Expression<Func<Product, bool>> where);
        Product Get(Expression<Func<Product, bool>> where);
        IEnumerable<Product> GetMany(Expression<Func<Product, bool>> where);
    }
}
