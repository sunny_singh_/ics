﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IProductCategoryService
    {
        IEnumerable<ProductCategory> GetAll();
        ProductCategory GetById(Guid id);
        int Insert(ProductCategory entity);
        int Update(ProductCategory entity);
        int Delete(Guid id);
        int Delete(ProductCategory entity);
        int Delete(Expression<Func<ProductCategory, bool>> where);
        ProductCategory Get(Expression<Func<ProductCategory, bool>> where);
        IEnumerable<ProductCategory> GetMany(Expression<Func<ProductCategory, bool>> where);
    }
}
