﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IProductTypeService
    {
        IEnumerable<ProductType> GetAll();
        ProductType GetById(Guid id);
        int Insert(ProductType entity);
        int Update(ProductType entity);
        int Delete(Guid id);
        int Delete(ProductType entity);
        int Delete(Expression<Func<ProductType, bool>> where);
        ProductType Get(Expression<Func<ProductType, bool>> where);
        IEnumerable<ProductType> GetMany(Expression<Func<ProductType, bool>> where);
    }
}
