﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
   public interface IEmployeeService
    {
        IEnumerable<Employee> GetAll();
        Employee GetById(Guid id);
        int Insert(Employee entity);
        int Update(Employee entity);
        int Delete(Guid id);
        int Delete(Employee entity);
        int Delete(Expression<Func<Employee, bool>> where);
        Employee Get(Expression<Func<Employee, bool>> where);
        IEnumerable<Employee> GetMany(Expression<Func<Employee, bool>> where);
    }
}
