﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IStateService
    {
        IEnumerable<State> GetAll();
        State GetById(Guid id);
        int Insert(State entity);
        int Update(State entity);
        int Delete(Guid id);
        int Delete(State entity);
        int Delete(Expression<Func<State, bool>> where);
        State Get(Expression<Func<State, bool>> where);
        IEnumerable<State> GetMany(Expression<Func<State, bool>> where);
    }
}
