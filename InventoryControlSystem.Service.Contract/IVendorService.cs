﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IVendorService
    {
        IEnumerable<Vendor> GetAll();
        Vendor GetById(Guid id);
        int Insert(Vendor entity);
        int Update(Vendor entity);
        int Delete(Guid id);
        int Delete(Vendor entity);
        int Delete(Expression<Func<Vendor, bool>> where);
        Vendor Get(Expression<Func<Vendor, bool>> where);
        IEnumerable<Vendor> GetMany(Expression<Func<Vendor, bool>> where);
    }
}
