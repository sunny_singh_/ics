﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
   public interface IOrganisationService
    {
        IEnumerable<Organisation> GetAll();
        Organisation GetById(Guid id);
        int Insert(Organisation entity);
        int Update(Organisation entity);
        int Delete(Guid id);
        int Delete(Organisation entity);
        int Delete(Expression<Func<Organisation, bool>> where);
        Organisation Get(Expression<Func<Organisation, bool>> where);
        IEnumerable<Organisation> GetMany(Expression<Func<Organisation, bool>> where);
    }
}