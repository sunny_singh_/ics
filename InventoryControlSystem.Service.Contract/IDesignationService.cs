﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
  public  interface IDesignationService
    {

        IEnumerable<Designation> GetAll();
        Designation GetById(Guid id);
        int Insert(Designation entity);
        int Update(Designation entity);
        int Delete(Guid id);
        int Delete(Designation entity);
        int Delete(Expression<Func<Designation, bool>> where);
        Designation Get(Expression<Func<Designation, bool>> where);
    }
}
