﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IMeasurementService
    {
        IEnumerable<Measurement> GetAll();
        Measurement GetById(Guid id);
        int Insert(Measurement entity);
        int Update(Measurement entity);
        int Delete(Guid id);
        int Delete(Measurement entity);
        int Delete(Expression<Func<Measurement, bool>> where);
        Measurement Get(Expression<Func<Measurement, bool>> where);
        IEnumerable<Measurement> GetMany(Expression<Func<Measurement, bool>> where);
    }
}
