﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service.Contract
{
    public interface IBrandService
    {
        IEnumerable<Brand> GetAll();
        Brand GetById(Guid id);
        int Insert(Brand entity);
        int Update(Brand entity);
        int Delete(Guid id);
        int Delete(Brand entity);
        int Delete(Expression<Func<Brand, bool>> where);
        Brand Get(Expression<Func<Brand, bool>> where);
        IEnumerable<Brand> GetMany(Expression<Func<Brand, bool>> where);
    }
}
