﻿using InventoryControlSystem.DAL.Contract.Repository;
using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.DAL.Contract.UnitOfWork
{
    public interface IUnitOfWork<U> where U : DbContext, IDisposable
    {
        int Commit();
        Task<int> CommitAsync();

        /// <summary>
        /// Repository intefaces
        /// </summary>
        IDepartmentRepository DepartmentRepository { get; }
        IGenericRepository<Brand, Guid> BrandRepository { get; }
        IGenericRepository<ProductCategory, Guid> ProductCategoryRepository { get; }
        IGenericRepository<ProductType, Guid> ProductTypeRepository { get; }
        IGenericRepository<Measurement, Guid> MeasurementRepository { get; }
        IGenericRepository<State, Guid> StateRepository { get; }
        IGenericRepository<Stock, Guid> StockRepository { get; }
        IGenericRepository<FinancialYear,Guid> FinancialYearRepository { get; }
        IGenericRepository<Location,Guid> LocationRepository { get; }
        IGenericRepository<Drawer,Guid> DrawerRepository { get; }
        IGenericRepository<Organisation,Guid> OrganisationRepository { get; }

        IGenericRepository<Employee,Guid> EmployeeRepository { get; }
        IGenericRepository<Vendor,Guid> VendorRepository { get; }
        IGenericRepository<Product,Guid> ProductRepository { get; }

        IGenericRepository<Designation,Guid> DesignationRepository { get; }
    }
}
