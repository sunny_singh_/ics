﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.DAL.Contract.Repository
{
    public interface IDepartmentRepository : IGenericRepository<Department, Guid>
    {
        IEnumerable<Department> GetDepartment();

    }
}
