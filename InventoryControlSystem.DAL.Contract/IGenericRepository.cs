﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace InventoryControlSystem.DAL.Contract.Repository
{
    public interface IGenericRepository <TEntity,TId>
        where TEntity : class
        where TId : struct
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(TId id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TId id);
        void Delete(TEntity entity);
        void Delete(Expression<Func<TEntity, bool>> where);
        TEntity Get(Expression<Func<TEntity, bool>> where);
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where);
    }
}

