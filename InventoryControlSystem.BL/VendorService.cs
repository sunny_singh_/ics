﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class VendorService : IVendorService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public VendorService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Vendor> GetAll()
        {
            var ds = _unitOfWork.VendorRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Vendor GetById(Guid id)
        {
            return _unitOfWork.VendorRepository.GetById(id);
        }



        public int Insert(Vendor entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.VendorRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Vendor entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.VendorRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.VendorRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Vendor entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.VendorRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Vendor, bool>> @where)
        {
            _unitOfWork.VendorRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Vendor Get(Expression<Func<Vendor, bool>> @where)
        {
            return _unitOfWork.VendorRepository.Get(@where);
        }

        public IEnumerable<Vendor> GetMany(Expression<Func<Vendor, bool>> @where)
        {
            return _unitOfWork.VendorRepository.GetMany(@where);
        }


    }
}
