﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service
{
    
   public class OrganisationService: IOrganisationService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public OrganisationService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Organisation> GetAll()
        {
            var ds = _unitOfWork.OrganisationRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Organisation GetById(Guid id)
        {
            return _unitOfWork.OrganisationRepository.GetById(id);
        }



        public int Insert(Organisation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.OrganisationRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Organisation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.OrganisationRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.OrganisationRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Organisation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.OrganisationRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Organisation, bool>> @where)
        {
            _unitOfWork.OrganisationRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Organisation Get(Expression<Func<Organisation, bool>> @where)
        {
            return _unitOfWork.OrganisationRepository.Get(@where);
        }

        public IEnumerable<Organisation> GetMany(Expression<Func<Organisation, bool>> @where)
        {
            return _unitOfWork.OrganisationRepository.GetMany(@where);
        }


    }
}

