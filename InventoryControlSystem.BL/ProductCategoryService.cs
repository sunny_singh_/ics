﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public ProductCategoryService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ProductCategory> GetAll()
        {
            var ds = _unitOfWork.ProductCategoryRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public ProductCategory GetById(Guid id)
        {
            return _unitOfWork.ProductCategoryRepository.GetById(id);
        }



        public int Insert(ProductCategory entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductCategoryRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(ProductCategory entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductCategoryRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.ProductCategoryRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(ProductCategory entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductCategoryRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<ProductCategory, bool>> @where)
        {
            _unitOfWork.ProductCategoryRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public ProductCategory Get(Expression<Func<ProductCategory, bool>> @where)
        {
            return _unitOfWork.ProductCategoryRepository.Get(@where);
        }

        public IEnumerable<ProductCategory> GetMany(Expression<Func<ProductCategory, bool>> @where)
        {
            return _unitOfWork.ProductCategoryRepository.GetMany(@where);
        }


    }
}
