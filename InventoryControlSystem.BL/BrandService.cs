﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public BrandService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Brand> GetAll()
        {
            var ds = _unitOfWork.BrandRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Brand GetById(Guid id)
        {
            return _unitOfWork.BrandRepository.GetById(id);
        }



        public int Insert(Brand entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.BrandRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Brand entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.BrandRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.BrandRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Brand entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.BrandRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Brand, bool>> @where)
        {
            _unitOfWork.BrandRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Brand Get(Expression<Func<Brand, bool>> @where)
        {
            return _unitOfWork.BrandRepository.Get(@where);
        }

        public IEnumerable<Brand> GetMany(Expression<Func<Brand, bool>> @where)
        {
            return _unitOfWork.BrandRepository.GetMany(@where);
        }


    }
}
