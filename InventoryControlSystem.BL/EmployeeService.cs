﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service
{
  public  class EmployeeService: IEmployeeService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public EmployeeService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Employee> GetAll()
        {
            var ds = _unitOfWork.EmployeeRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Employee GetById(Guid id)
        {
            return _unitOfWork.EmployeeRepository.GetById(id);
        }



        public int Insert(Employee entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.EmployeeRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Employee entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.EmployeeRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.EmployeeRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Employee entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.EmployeeRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Employee, bool>> @where)
        {
            _unitOfWork.EmployeeRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Employee Get(Expression<Func<Employee, bool>> @where)
        {
            return _unitOfWork.EmployeeRepository.Get(@where);
        }

        public IEnumerable<Employee> GetMany(Expression<Func<Employee, bool>> @where)
        {
            return _unitOfWork.EmployeeRepository.GetMany(@where);
        }


    }
}
