﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class MeasurementService : IMeasurementService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public MeasurementService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Measurement> GetAll()
        {
            var ds = _unitOfWork.MeasurementRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Measurement GetById(Guid id)
        {
            return _unitOfWork.MeasurementRepository.GetById(id);
        }



        public int Insert(Measurement entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.MeasurementRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Measurement entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.MeasurementRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.MeasurementRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Measurement entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.MeasurementRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Measurement, bool>> @where)
        {
            _unitOfWork.MeasurementRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Measurement Get(Expression<Func<Measurement, bool>> @where)
        {
            return _unitOfWork.MeasurementRepository.Get(@where);
        }

        public IEnumerable<Measurement> GetMany(Expression<Func<Measurement, bool>> @where)
        {
            return _unitOfWork.MeasurementRepository.GetMany(@where);
        }


    }
}
