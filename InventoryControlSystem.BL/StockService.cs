﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class StockService : IStockService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public StockService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Stock> GetAll()
        {
            var ds = _unitOfWork.StockRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Stock GetById(Guid id)
        {
            return _unitOfWork.StockRepository.GetById(id);
        }



        public int Insert(Stock entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StockRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Stock entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StockRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.StockRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Stock entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StockRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Stock, bool>> @where)
        {
            _unitOfWork.StockRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Stock Get(Expression<Func<Stock, bool>> @where)
        {
            return _unitOfWork.StockRepository.Get(@where);
        }

        public IEnumerable<Stock> GetMany(Expression<Func<Stock, bool>> @where)
        {
            return _unitOfWork.StockRepository.GetMany(@where);
        }


    }
}
