﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class StateService : IStateService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public StateService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<State> GetAll()
        {
            var ds = _unitOfWork.StateRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public State GetById(Guid id)
        {
            return _unitOfWork.StateRepository.GetById(id);
        }



        public int Insert(State entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StateRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(State entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StateRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.StateRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(State entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.StateRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<State, bool>> @where)
        {
            _unitOfWork.StateRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public State Get(Expression<Func<State, bool>> @where)
        {
            return _unitOfWork.StateRepository.Get(@where);
        }

        public IEnumerable<State> GetMany(Expression<Func<State, bool>> @where)
        {
            return _unitOfWork.StateRepository.GetMany(@where);
        }


    }
}
