﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace InventoryControlSystem.Service
{
    public class LocationService : ILocationService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public LocationService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Location> GetAll()
        {
            var ds = _unitOfWork.LocationRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Location GetById(Guid id)
        {
            return _unitOfWork.LocationRepository.GetById(id);
        }



        public int Insert(Location entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.LocationRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Location entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.LocationRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.LocationRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Location entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.LocationRepository.Delete(entity);
            return _unitOfWork.Commit();
        }



        public int Delete(Expression<Func<Location, bool>> where)
        {
            _unitOfWork.LocationRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Location Get(Expression<Func<Location, bool>> where)
        {
            return _unitOfWork.LocationRepository.Get(@where);
        }

        public IEnumerable<Location> GetMany(Expression<Func<Location, bool>> where)
        {
            return _unitOfWork.LocationRepository.GetMany(@where);
        }
    }
}
