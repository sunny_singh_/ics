﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace InventoryControlSystem.Service
{
   public class FinancialYearService: IFinancialYearService
    {
           private readonly IUnitOfWork<ICSContext> _unitOfWork;

            public FinancialYearService(IUnitOfWork<ICSContext> unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public IEnumerable<FinancialYear> GetAll()
            {
                var ds = _unitOfWork.FinancialYearRepository.GetAll();
                _unitOfWork.Commit();
                return ds;
            }

            public FinancialYear GetById(Guid id)
            {
                return _unitOfWork.FinancialYearRepository.GetById(id);
            }



            public int Insert(FinancialYear entity)
            {
                if (entity == null) throw new ArgumentNullException("entity");
                _unitOfWork.FinancialYearRepository.Insert(entity);
                return _unitOfWork.Commit();
            }

            public int Update(FinancialYear entity)
            {
                if (entity == null) throw new ArgumentNullException("entity");
                _unitOfWork.FinancialYearRepository.Update(entity);
                return _unitOfWork.Commit();
            }

            public int Delete(Guid id)
            {
                _unitOfWork.FinancialYearRepository.Delete(id);
                return _unitOfWork.Commit();
            }

            public int Delete(FinancialYear entity)
            {
                if (entity == null) throw new ArgumentNullException("entity");
                _unitOfWork.FinancialYearRepository.Delete(entity);
                return _unitOfWork.Commit();
            }

            public int Delete(Expression<Func<FinancialYear, bool>> @where)
            {
                _unitOfWork.FinancialYearRepository.Delete(@where);
                return _unitOfWork.Commit();
            }

            public FinancialYear Get(Expression<Func<FinancialYear, bool>> @where)
            {
                return _unitOfWork.FinancialYearRepository.Get(@where);
            }

            public IEnumerable<FinancialYear> GetMany(Expression<Func<FinancialYear, bool>> @where)
            {
                return _unitOfWork.FinancialYearRepository.GetMany(@where);
            }

       
    }
    }
