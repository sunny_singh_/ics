﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Service
{
  public  class DesignationService: IDesignationService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public DesignationService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Designation> GetAll()
        {
            var ds = _unitOfWork.DesignationRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Designation GetById(Guid id)
        {
            return _unitOfWork.DesignationRepository.GetById(id);
        }



        public int Insert(Designation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DesignationRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Designation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DesignationRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.DesignationRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Designation entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DesignationRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Designation, bool>> @where)
        {
            _unitOfWork.DesignationRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Designation Get(Expression<Func<Designation, bool>> @where)
        {
            return _unitOfWork.DesignationRepository.Get(@where);
        }

        public IEnumerable<Designation> GetMany(Expression<Func<Designation, bool>> @where)
        {
            return _unitOfWork.DesignationRepository.GetMany(@where);
        }


    }
}