﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public ProductTypeService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ProductType> GetAll()
        {
            var ds = _unitOfWork.ProductTypeRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public ProductType GetById(Guid id)
        {
            return _unitOfWork.ProductTypeRepository.GetById(id);
        }



        public int Insert(ProductType entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductTypeRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(ProductType entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductTypeRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.ProductTypeRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(ProductType entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductTypeRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<ProductType, bool>> @where)
        {
            _unitOfWork.ProductTypeRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public ProductType Get(Expression<Func<ProductType, bool>> @where)
        {
            return _unitOfWork.ProductTypeRepository.Get(@where);
        }

        public IEnumerable<ProductType> GetMany(Expression<Func<ProductType, bool>> @where)
        {
            return _unitOfWork.ProductTypeRepository.GetMany(@where);
        }


    }
}
