﻿using InventoryControlSystem.DAL.Context;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace InventoryControlSystem.Service
{
   public class DrawerService: IDrawerService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public DrawerService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Drawer> GetAll()
        {
            var ds = _unitOfWork.DrawerRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Drawer GetById(Guid id)
        {
            return _unitOfWork.DrawerRepository.GetById(id);
        }



        public int Insert(Drawer entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DrawerRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Drawer entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DrawerRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.DrawerRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Drawer entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DrawerRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Drawer, bool>> @where)
        {
            _unitOfWork.DrawerRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Drawer Get(Expression<Func<Drawer, bool>> @where)
        {
            return _unitOfWork.DrawerRepository.Get(@where);
        }

        public IEnumerable<Drawer> GetMany(Expression<Func<Drawer, bool>> @where)
        {
            return _unitOfWork.DrawerRepository.GetMany(@where);
        }

       
    }
}