﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public DepartmentService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Department> GetAll()
        {
            var ds = _unitOfWork.DepartmentRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Department GetById(Guid id)
        {
            return _unitOfWork.DepartmentRepository.GetById(id);
        }

        

        public int Insert(Department entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DepartmentRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Department entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DepartmentRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.DepartmentRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Department entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.DepartmentRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Department, bool>> @where)
        {
            _unitOfWork.DepartmentRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Department Get(Expression<Func<Department, bool>> @where)
        {
            return _unitOfWork.DepartmentRepository.Get(@where);
        }

        public IEnumerable<Department> GetMany(Expression<Func<Department, bool>> @where)
        {
            return _unitOfWork.DepartmentRepository.GetMany(@where);
        }

     
    }
    }
