﻿using InventoryControlSystem.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.Entity;
using System.Linq.Expressions;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Context;

namespace InventoryControlSystem.BL
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork<ICSContext> _unitOfWork;

        public ProductService(IUnitOfWork<ICSContext> unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Product> GetAll()
        {
            var ds = _unitOfWork.ProductRepository.GetAll();
            _unitOfWork.Commit();
            return ds;
        }

        public Product GetById(Guid id)
        {
            return _unitOfWork.ProductRepository.GetById(id);
        }



        public int Insert(Product entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductRepository.Insert(entity);
            return _unitOfWork.Commit();
        }

        public int Update(Product entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductRepository.Update(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Guid id)
        {
            _unitOfWork.ProductRepository.Delete(id);
            return _unitOfWork.Commit();
        }

        public int Delete(Product entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            _unitOfWork.ProductRepository.Delete(entity);
            return _unitOfWork.Commit();
        }

        public int Delete(Expression<Func<Product, bool>> @where)
        {
            _unitOfWork.ProductRepository.Delete(@where);
            return _unitOfWork.Commit();
        }

        public Product Get(Expression<Func<Product, bool>> @where)
        {
            return _unitOfWork.ProductRepository.Get(@where);
        }

        public IEnumerable<Product> GetMany(Expression<Func<Product, bool>> @where)
        {
            return _unitOfWork.ProductRepository.GetMany(@where);
        }


    }
}
