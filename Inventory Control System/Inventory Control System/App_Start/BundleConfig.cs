﻿using System.Web;
using System.Web.Optimization;

namespace InventoryControlSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Scripts/bootstrap.js",
                      "~/Theme/plugins/jQuery/jQuery-2.1.4.min.js",
                      "~/Theme/bootstrap/js/bootstrap.min.js",
                      "~/Theme/plugins/datatables/jquery.dataTables.min.js",
                      "~/Theme/plugins/datatables/dataTables.bootstrap.js",
                      "~/Theme/plugins/fastclick/fastclick.min.js",
                      "~/Theme/dist/js/app.min.js",
                      "~/Theme/plugins/sparkline/jquery.sparkline.min.js",
                      "~/Theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                      "~/Theme/plugins/slimScroll/jquery.slimscroll.min.js",
                      "~/Theme/plugins/chartjs/Chart.min.js",
                      //"~/Theme/dist/js/pages/dashboard2.js",
                      "~/Theme/dist/js/demo.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Theme/bootstrap/css/bootstrap.min.css",
                      "~/Theme/plugins/datatables/dataTables.bootstrap.css",
                      "~/Theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                      "~/Theme/dist/css/AdminLTE.min.css",
                      "~/Theme/dist/css/skins/_all-skins.min.css",
                      "~/Content/site.css"));
        }
    }
}
