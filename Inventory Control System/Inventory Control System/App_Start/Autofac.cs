﻿using Autofac;
using Autofac.Integration.Mvc;
using InventoryControlSystem.BL;
using InventoryControlSystem.DAL;
using InventoryControlSystem.DAL.Contract.Repository;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.UnitOfWork;
using InventoryControlSystem.Service;
using InventoryControlSystem.Service.Contract;
using System.Reflection;
using System.Web.Mvc;

namespace InventoryControlSystem.App_Start
{
    public static class AutofacConfig
    {
        public static void Run()
        {
            SetAutofacContainer();
            //Configure AutoMapper
            //  AutoMapperConfiguration.Configure();
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterGeneric(typeof(UnitOfWork<>)).As(typeof(IUnitOfWork<>));
            builder.RegisterGeneric(typeof(GenericRepository<,>)).As(typeof(IGenericRepository<,>));
            builder.RegisterType(typeof(DepartmentService)).As(typeof(IDepartmentService)).InstancePerDependency();
            builder.RegisterType(typeof(BrandService)).As(typeof(IBrandService)).InstancePerDependency();
            builder.RegisterType(typeof(ProductCategoryService)).As(typeof(IProductCategoryService)).InstancePerDependency();
            builder.RegisterType(typeof(ProductTypeService)).As(typeof(IProductTypeService)).InstancePerDependency();
            builder.RegisterType(typeof(MeasurementService)).As(typeof(IMeasurementService)).InstancePerDependency();
            builder.RegisterType(typeof(StateService)).As(typeof(IStateService)).InstancePerDependency();
            builder.RegisterType(typeof(StockService)).As(typeof(IStockService)).InstancePerDependency();

            builder.RegisterType(typeof(FinancialYearService)).As(typeof(IFinancialYearService)).InstancePerDependency();
            builder.RegisterType(typeof(LocationService)).As(typeof(ILocationService)).InstancePerDependency();
            builder.RegisterType(typeof(DrawerService)).As(typeof(IDrawerService)).InstancePerDependency();
            builder.RegisterType(typeof(OrganisationService)).As(typeof(IOrganisationService)).InstancePerDependency();
            builder.RegisterType(typeof(EmployeeService)).As(typeof(IEmployeeService)).InstancePerDependency();
            builder.RegisterType(typeof(VendorService)).As(typeof(IVendorService)).InstancePerDependency();
            builder.RegisterType(typeof(ProductService)).As(typeof(IProductService)).InstancePerDependency();
            builder.RegisterType(typeof(DesignationService)).As(typeof(IDesignationService)).InstancePerDependency();
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
