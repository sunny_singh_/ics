﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InventoryControlSystem.App_Start
{
    public class AutomapperConfig
    {

        public static void Run()
        {
            Mapper.CreateMap<Brand, BrandViewModel>()
            .ForMember(x=>x.UserName,opts=>opts.MapFrom(src=>src.ApplicationUsers.UserName));
            Mapper.CreateMap<BrandViewModel, Brand>();

            Mapper.CreateMap<Department, DepartmentViewModel>()
            .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<DepartmentViewModel, Department>();
            Mapper.CreateMap<ProductCategoryViewModel, ProductCategory>();
            Mapper.CreateMap<ProductCategory, ProductCategoryViewModel>()
            .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<ProductTypeViewModel, ProductType>();
            Mapper.CreateMap<ProductType, ProductTypeViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<MeasurementViewModel, Measurement>();
            Mapper.CreateMap<Measurement, MeasurementViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<StateViewModel, State>();
            Mapper.CreateMap<State, StateViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<StockViewModel, Stock>();
            Mapper.CreateMap<Stock, StockViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));

            Mapper.CreateMap<Brand, BrandViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUsers.UserName));
            Mapper.CreateMap<BrandViewModel, Brand>();

            Mapper.CreateMap<Department, DepartmentViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<DepartmentViewModel, Department>();

            Mapper.CreateMap<FinancialYearViewModel, FinancialYear>();
            Mapper.CreateMap<FinancialYear, FinancialYearViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));

            Mapper.CreateMap<LocationViewModel, Location>();
            Mapper.CreateMap<Location, LocationViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));

            Mapper.CreateMap<Drawer, DrawerViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<DrawerViewModel, Drawer>();

            Mapper.CreateMap<Organisation, OrganisationViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<OrganisationViewModel, Organisation>();

            Mapper.CreateMap<Employee, EmployeeViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<EmployeeViewModel, Employee>();


            Mapper.CreateMap<Vendor, VendorViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName));
            Mapper.CreateMap<VendorViewModel, Vendor>();

            Mapper.CreateMap<Product, ProductViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUser.UserName))
                .ForMember(x=>x.ProductType,opts=>opts.MapFrom(src=>src.ProductType.PType))
                .ForMember(x=>x.Brand, opt=>opt.MapFrom(src=>src.Brand.BrandName))
                .ForMember(x=>x.Category, opt=>opt.MapFrom(src=>src.ProductCatgory.PCategory))
                .ForMember(x=>x.Drawer,opt=>opt.MapFrom(src=>src.Drawer.DrawerNo))
                .ForMember(x=>x.Stock,opt=>opt.MapFrom(src=>src.Stocks.Count));
            Mapper.CreateMap<ProductViewModel, Product>();

            Mapper.CreateMap<Designation, DesignationViewModel>()
                .ForMember(x => x.UserName, opts => opts.MapFrom(src => src.ApplicationUsers.UserName));
            Mapper.CreateMap<DesignationViewModel, Designation>();

        }
    }
}