﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InventoryControlSystem.Startup))]
namespace InventoryControlSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
