﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class ProductCategoryController : Controller
    {
        private readonly IProductCategoryService _productCategoryService;

        public ProductCategoryController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var productCategories = _productCategoryService.GetAll();
            var productCategoriesList = Mapper.Map<IEnumerable<ProductCategory>, IEnumerable<ProductCategoryViewModel>>(productCategories);
            return View(productCategoriesList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var productCategory = _productCategoryService.GetById(id);
            var productCategoryModel = Mapper.Map<ProductCategory, ProductCategoryViewModel>(productCategory);
            return View(productCategoryModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            var model = new ProductCategoryViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(ProductCategoryViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var productCategory = Mapper.Map<ProductCategoryViewModel, ProductCategory>(model);
                productCategory.CreatedTime = DateTime.Now;
                productCategory.UserId = User.Identity.GetUserId();
                _productCategoryService.Insert(productCategory);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {

            var productCategory = _productCategoryService.GetById(id);
            var productCategoryModel = Mapper.Map<ProductCategory, ProductCategoryViewModel>(productCategory);
            return View(productCategoryModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(ProductCategoryViewModel model)
        {

            try
            {
               
                if(!ModelState.IsValid)
                {
                    return View(model);
                }
                var productCategory = Mapper.Map<ProductCategoryViewModel, ProductCategory>(model);
                _productCategoryService.Update(productCategory);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var productCategory = _productCategoryService.GetById(id);
                _productCategoryService.Delete(productCategory);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, ProductCategory model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var productCategory = _productCategoryService.GetById(id);
                _productCategoryService.Delete(productCategory);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
