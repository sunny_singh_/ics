﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class MeasurementController : Controller
    {
        private readonly IMeasurementService _measurementService;

        public MeasurementController(IMeasurementService measurementService)
        {
            _measurementService = measurementService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var measurements = _measurementService.GetAll();
            var measurementsList = Mapper.Map<IEnumerable<Measurement>, IEnumerable<MeasurementViewModel>>(measurements);
            return View(measurementsList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var measurement = _measurementService.GetById(id);
            var measurementModel = Mapper.Map<Measurement, MeasurementViewModel>(measurement);
            return View(measurementModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            var model = new MeasurementViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(MeasurementViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var measurement = Mapper.Map<MeasurementViewModel, Measurement>(model);
                measurement.CreatedTime = DateTime.Now;
                measurement.UserId = User.Identity.GetUserId();
                _measurementService.Insert(measurement);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {

            var measurement = _measurementService.GetById(id);
            var measurementModel = Mapper.Map<Measurement, MeasurementViewModel>(measurement);
            return View(measurementModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(MeasurementViewModel model)
        {

            try
            {
                if(!ModelState.IsValid)
                    return View(model);
                var measurement = Mapper.Map<MeasurementViewModel, Measurement>(model);
                _measurementService.Update(measurement);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var measurement = _measurementService.GetById(id);
                _measurementService.Delete(measurement);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Measurement model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var measurement = _measurementService.GetById(id);
                _measurementService.Delete(measurement);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
