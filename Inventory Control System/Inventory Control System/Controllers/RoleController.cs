﻿using InventoryControlSystem.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class RoleController : Controller
    {

        private ApplicationRoleManager _roleManager;

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }


        // GET: Role
        public ActionResult Index()
        {
            var roles = RoleManager.Roles.ToList().Select(role => new CreateRoleViewModel()
            {
                Id = role.Id,
                Name = role.Name
            });
            return View(roles);
        }

        //// GET: Role/Details/5
        //public ActionResult Details(int id)
        //{

        //    return View();
        //}

        // GET: Role/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Role/Create
        [HttpPost]
        public async Task<ActionResult> Create(CreateRoleViewModel role)
        {
            try
            {
                await RoleManager.CreateAsync(new IdentityRole(role.Name));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Role/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            return View(new CreateRoleViewModel() { Id = role.Id, Name = role.Name });
        }

        // POST: Role/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(CreateRoleViewModel entity)
        {
            try
            {
                var role = await RoleManager.FindByIdAsync(entity.Id);
                role.Name = entity.Name;
                await RoleManager.UpdateAsync(role);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(entity);
            }
        }

        // GET: Role/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            return View(new CreateRoleViewModel() { Id = role.Id, Name = role.Name });

        }

        // POST: Role/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(string id, FormCollection collection)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return RedirectToAction("Index");
                var role = await RoleManager.FindByIdAsync(id);
                await RoleManager.DeleteAsync(role);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
