﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var brands = _brandService.GetAll();
            var brandModelList = Mapper.Map<IEnumerable<Brand>, IEnumerable<BrandViewModel>>(brands);
            return View(brandModelList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var brand = _brandService.GetById(id);
            var brandModel = Mapper.Map<Brand, BrandViewModel>(brand);
            return View(brandModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            var model = new BrandViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(BrandViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var brand = Mapper.Map<BrandViewModel, Brand>(model);
                brand.UserId = User.Identity.GetUserId();
                brand.Createdtime = DateTime.Now;
                _brandService.Insert(brand);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {

            var brand = _brandService.GetById(id);
            var brandModel = Mapper.Map<Brand, BrandViewModel>(brand);
            return View(brandModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(BrandViewModel model)
        {

            try
            {
                //if (id == null)
                //    return RedirectToAction("Index");
                var brand = Mapper.Map<BrandViewModel, Brand>(model);
                _brandService.Update(brand);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id== Guid.Empty)
                    return RedirectToAction("Index");
                var brand = _brandService.GetById(id);
                _brandService.Delete(brand);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Brand model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var brand = _brandService.GetById(id);
                _brandService.Delete(brand);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
