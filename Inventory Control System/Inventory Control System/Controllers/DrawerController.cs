﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class DrawerController : Controller
    {

        private readonly IDrawerService _drawerService;
        private readonly ILocationService _locationService;

        public DrawerController(IDrawerService drawerService, ILocationService locationService)
        {
            _drawerService = drawerService;
            _locationService = locationService;
        }
        // GET: Drawer
        public ActionResult Index()
        {
            var drawer = _drawerService.GetAll();
            var drawerModelList = Mapper.Map<IEnumerable<Drawer>, IEnumerable<DrawerViewModel>>(drawer);

            return View(drawerModelList);
        }

        // GET: Drawer/Details/5
        public ActionResult Details(Guid id)
        {
            return View();
        }

        // GET: Drawer/Create
        public ActionResult Create()
        {
            ViewBag.Locations = _locationService.GetAll().ToList();
            return View();
        }

        // POST: Drawer/Create
        [HttpPost]
        public ActionResult Create(DrawerViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var drawer = Mapper.Map<DrawerViewModel, Drawer>(model);
                drawer.UserId = User.Identity.GetUserId();
                drawer.CreatedTime = DateTime.Now;
                _drawerService.Insert(drawer);
                return RedirectToAction("Index");               
            }
            catch
            {
                ViewBag.Location = _locationService.GetAll();
                return View(model);
            }
        }

        // GET: Drawer/Edit/5
        public ActionResult Edit(Guid id)
        {
            ViewBag.Locations = _locationService.GetAll().ToList();
            var drawer = _drawerService.GetById(id);
            var drawerModel = Mapper.Map<Drawer, DrawerViewModel>(drawer);
            return View(drawerModel);
        }

        // POST: Drawer/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, DrawerViewModel model)
        {
            try
            {
                ViewBag.Locations = _locationService.GetAll().ToList();
                if (!ModelState.IsValid)
                    return View(model);
                var drawer = Mapper.Map<DrawerViewModel, Drawer>(model);
                drawer.UserId = User.Identity.GetUserId();
                drawer.CreatedTime = DateTime.Now;
                _drawerService.Update(drawer);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Drawer/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var drawer = _drawerService.GetById(id);
                _drawerService.Delete(drawer);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        //// POST: Drawer/Delete/5
        //[HttpPost]
        //public ActionResult Delete(Guid id, DrawerViewModel collection)
        //{
           
        //}
    }
}
