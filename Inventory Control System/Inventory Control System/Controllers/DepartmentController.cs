﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }
        // GET: Department
        public ActionResult Index()
        {
            var departments = _departmentService.GetAll();
            var departmentModelList = Mapper.Map<IEnumerable<Department>, IEnumerable<DepartmentViewModel>>(departments);
            return View(departmentModelList);
        }

        // GET: Department/Details/5
        public ActionResult Details(Guid id)
        {
            var department = _departmentService.GetById(id);
            var departmentModel = Mapper.Map<Department, DepartmentViewModel>(department);
            return View(departmentModel);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            var model = new DepartmentViewModel();
            return View(model);
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(DepartmentViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var department = Mapper.Map<DepartmentViewModel, Department>(model);
                department.CreatedTime = DateTime.Now;
                department.UserId = User.Identity.GetUserId();
                _departmentService.Insert(department);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        // GET: Department/Edit/5
        public ActionResult Edit(Guid id)
        {

            var department = _departmentService.GetById(id);
            var departmentModel = Mapper.Map<Department, DepartmentViewModel>(department);
            return View(departmentModel);
        }

        // POST: Department/Edit/5
        [HttpPost]
        public ActionResult Edit(DepartmentViewModel model)
        {

            try
            {
                //if (id == null)
                //    return RedirectToAction("Index");
                var department = Mapper.Map<DepartmentViewModel, Department>(model);
                _departmentService.Update(department);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Department/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id==Guid.Empty)
                    return RedirectToAction("Index");
                var department = _departmentService.GetById(id);
                _departmentService.Delete(department);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Department model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var department = _departmentService.GetById(id);
                _departmentService.Delete(department);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
