﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class FinancialYearController : Controller
    {
        private readonly IFinancialYearService _financialYearService;

        public FinancialYearController(IFinancialYearService FinancialYearService)
        {
            _financialYearService = FinancialYearService;
        }
        // GET: FinancialYear
        public ActionResult Index()
        {
            var financialYears = _financialYearService.GetAll();
            var financialYearModelList = Mapper.Map<IEnumerable<FinancialYear>, IEnumerable<FinancialYearViewModel>>(financialYears);
            return View(financialYearModelList);

        }

        // GET: FinancialYear/Details/5 
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FinancialYear/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FinancialYear/Create
        [HttpPost]
        public ActionResult Create(FinancialYearViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var financialYear = Mapper.Map<FinancialYearViewModel, FinancialYear>(model);
                financialYear.UserId = User.Identity.GetUserId();
                financialYear.CreatedDate = DateTime.Now;
                _financialYearService.Insert(financialYear);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: FinancialYear/Edit/5
        public ActionResult Edit(Guid id)
        {
            var financialYear = _financialYearService.GetById(id);
            var financialYearModel = Mapper.Map<FinancialYear, FinancialYearViewModel>(financialYear);           
            return View(financialYearModel);
        }

        // POST: FinancialYear/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, FinancialYearViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    View(model);
                }
                //if (id == null)
                //    return RedirectToAction("Index");
                var financialYear = Mapper.Map<FinancialYearViewModel, FinancialYear>(model);
                _financialYearService.Update(financialYear);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: FinancialYear/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var financialYear = _financialYearService.GetById(id);
                _financialYearService.Delete(financialYear);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: FinancialYear/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
