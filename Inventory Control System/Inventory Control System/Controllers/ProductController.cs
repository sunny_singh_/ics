﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class ProductController : Controller
    {

        private readonly IProductService _productService;
        private readonly IMeasurementService _measurementService;
        private readonly IProductTypeService _productTypeService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly IBrandService _brandService;
        private readonly IDrawerService _drawerService;
        private readonly IStockService _stockService;



        public ProductController(IProductService productService,
            IMeasurementService measurementService,
            IProductTypeService productTypeService,
            IProductCategoryService productCategoryService,
            IBrandService brandService,
            IDrawerService drawerService,
            IStockService stockService)
        {
            _productService = productService;
            _measurementService = measurementService;
            _productTypeService = productTypeService;
            _productCategoryService = productCategoryService;
            _brandService = brandService;
            _drawerService = drawerService;
            _stockService = stockService;
        }

        // GET: Product
        public ActionResult Index()
        {
            var products = _productService.GetAll();
            var productsList = Mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(products);
            return View(productsList);
        }

        // GET: Product/Details/5
        public ActionResult Details(Guid id)
        {
            var product = _productService.GetById(id);
            var productModel = Mapper.Map<Product, ProductViewModel>(product);
            return View(productModel);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            var model = new ProductViewModel();
            var measurements = _measurementService.GetAll();
            var measurementsViewModel = Mapper.Map<List<MeasurementViewModel>>(measurements);
            model.Measurements = measurementsViewModel;

            var productTypes = _productTypeService.GetAll();
            var productTypesViewModel = Mapper.Map<List<ProductTypeViewModel>>(productTypes);
            model.ProductTypes = productTypesViewModel;

            var productCategories = _productCategoryService.GetAll();
            var productCategoriesViewModel = Mapper.Map<List<ProductCategoryViewModel>>(productCategories);
            model.ProductCatgories = productCategoriesViewModel;

            var brands = _brandService.GetAll();
            var brandsViewModel = Mapper.Map<List<BrandViewModel>>(brands);
            model.Brands = brandsViewModel;

            var drawers = _drawerService.GetAll();
            var drawersViewModel = Mapper.Map<List<DrawerViewModel>>(drawers);
            model.Drawers = drawersViewModel;

            var stocks = _stockService.GetAll();
            var stocksViewModel = Mapper.Map<List<StockViewModel>>(stocks);
            model.Stocks = stocksViewModel;

            return View(model);
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var product = Mapper.Map<ProductViewModel, Product>(model);
                product.UserID = User.Identity.GetUserId();
                product.CreatedTime = DateTime.Now;
                _productService.Insert(product);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(Guid id)
        {

            var product = _productService.GetById(id);

            var model = Mapper.Map<Product, ProductViewModel>(product);
            var measurements = _measurementService.GetAll();
            var measurementsViewModel = Mapper.Map<List<MeasurementViewModel>>(measurements);
            model.Measurements = measurementsViewModel;

            var productTypes = _productTypeService.GetAll();
            var productTypesViewModel = Mapper.Map<List<ProductTypeViewModel>>(productTypes);
            model.ProductTypes = productTypesViewModel;

            var productCategories = _productCategoryService.GetAll();
            var productCategoriesViewModel = Mapper.Map<List<ProductCategoryViewModel>>(productCategories);
            model.ProductCatgories = productCategoriesViewModel;

            var brands = _brandService.GetAll();
            var brandsViewModel = Mapper.Map<List<BrandViewModel>>(brands);
            model.Brands = brandsViewModel;

            var drawers = _drawerService.GetAll();
            var drawersViewModel = Mapper.Map<List<DrawerViewModel>>(drawers);
            model.Drawers = drawersViewModel;

            var stocks = _stockService.GetAll();
            var stocksViewModel = Mapper.Map<List<StockViewModel>>(stocks);
            model.Stocks = stocksViewModel;
            return View(model);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {

            try
            {
                if(!ModelState.IsValid)
                {
                    return View(model);
                }
                var product = Mapper.Map<ProductViewModel, Product>(model);
                _productService.Update(product);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var product = _productService.GetById(id);
                _productService.Delete(product);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Product model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var product = _productService.GetById(id);
                _productService.Delete(product);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
