﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class OrganisationController : Controller
    {
        private readonly IOrganisationService _organisationService;
        private readonly IStateService _stateService;
        public OrganisationController(IOrganisationService organisationService,IStateService stateService)
        {
            _organisationService = organisationService;
            _stateService = stateService;
        }
        // GET: Organisation
        public ActionResult Index()
        {
            var orgnizations = _organisationService.GetAll();
            var orgnizationModelList = Mapper.Map<IEnumerable<Organisation>, IEnumerable<OrganisationViewModel>>(orgnizations);
            return View(orgnizationModelList);
        }

        // GET: Organisation/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Organisation/Create
        public ActionResult Create()
        {
            ViewBag.States = _stateService.GetAll();
            return View();
        }

        // POST: Organisation/Create
        [HttpPost]
        public ActionResult Create(OrganisationViewModel model)
        {
            try
            {
                ViewBag.States = _stateService.GetAll();
                if (!ModelState.IsValid)
                    return View(model);
                var org = Mapper.Map<OrganisationViewModel, Organisation>(model);
                org.UserId = User.Identity.GetUserId();
                org.CreatedTime = DateTime.Now;
                _organisationService.Insert(org);
                return RedirectToAction("Index");
            }
            catch
            {
              
                return View();
            }
        }

        // GET: Organisation/Edit/5
        public ActionResult Edit(Guid id)
        {
            ViewBag.States = _stateService.GetAll();
            var org = _organisationService.GetById(id);
            var orgModel = Mapper.Map<Organisation, OrganisationViewModel>(org);
            return View(orgModel);
        }

        // POST: 
        [HttpPost]
        public ActionResult Edit(Guid id, OrganisationViewModel model)
        {
            try
            {
                if(!ModelState.IsValid)
                {
                    ViewBag.States = _stateService.GetAll();
                    return View(model);
                }
                var org = Mapper.Map<OrganisationViewModel, Organisation>(model);
                _organisationService.Update(org);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: 
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var org = _organisationService.GetById(id);
                _organisationService.Delete(org);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

       
    }
}
