﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class ProductTypeController : Controller
    {
        private readonly IProductTypeService _productTypeService;

        public ProductTypeController(IProductTypeService productTypeService)
        {
            _productTypeService = productTypeService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var productTypes = _productTypeService.GetAll();
            var productTypesList = Mapper.Map<IEnumerable<ProductType>, IEnumerable<ProductTypeViewModel>>(productTypes);
            return View(productTypesList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var productType = _productTypeService.GetById(id);
            var productTypeModel = Mapper.Map<ProductType, ProductTypeViewModel>(productType);
            return View(productTypeModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            var model = new ProductTypeViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(ProductTypeViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var productType = Mapper.Map<ProductTypeViewModel, ProductType>(model);
                productType.UserId = User.Identity.GetUserId();
                productType.CreatedTime = DateTime.Now;
                _productTypeService.Insert(productType);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {

            var productType = _productTypeService.GetById(id);
            var productTypeModel = Mapper.Map<ProductType, ProductTypeViewModel>(productType);
            return View(productTypeModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(ProductTypeViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                    return View(model);
               
                var productType = Mapper.Map<ProductTypeViewModel, ProductType>(model);
                _productTypeService.Update(productType);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var productType = _productTypeService.GetById(id);
                _productTypeService.Delete(productType);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, ProductType model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var productType = _productTypeService.GetById(id);
                _productTypeService.Delete(productType);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
