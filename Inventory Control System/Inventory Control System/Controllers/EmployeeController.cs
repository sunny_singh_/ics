﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IStateService _stateService;
        private readonly IDepartmentService _departmentService;
        private readonly IDesignationService _designationService;
        public EmployeeController(IEmployeeService employeeService, IStateService stateService, 
            IDepartmentService departmentService, IDesignationService designationService)
        {
            _employeeService = employeeService;
           _stateService=stateService;
            _departmentService = departmentService;
            _designationService = designationService;
        }
        // GET: Employee
        public ActionResult Index()
        {
            var emps = _employeeService.GetAll();
            var empModelList = Mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeViewModel>>(emps);
            return View(empModelList);
           
        }

        // GET: Employee/Details/5
        public ActionResult Details(Guid id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            ViewBag.States = _stateService.GetAll().ToList();
            ViewBag.Departments = _departmentService.GetAll().ToList();
            ViewBag.Desginations = _designationService.GetAll().ToList();
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(EmployeeViewModel model)
        {
            try
            {
                ViewBag.States = _stateService.GetAll().ToList();
                ViewBag.Departments = _departmentService.GetAll().ToList();
                ViewBag.Desginations = _designationService.GetAll().ToList();
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var emp = Mapper.Map<EmployeeViewModel, Employee>(model);
                emp.UserId = User.Identity.GetUserId();
                emp.CreatedTime = DateTime.Now;
                _employeeService.Insert(emp);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View(model);
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(Guid id)
        {
           
            var emp = _employeeService.GetById(id);
            var empModel = Mapper.Map<Employee, EmployeeViewModel>(emp);
            ViewBag.States = _stateService.GetAll().ToList();
            ViewBag.Departments = _departmentService.GetAll().ToList();
            ViewBag.Desginations = _designationService.GetAll().ToList();
            return View(empModel);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, EmployeeViewModel model)
        {
            try
            {
              
                if(!ModelState.IsValid)
                {
                    ViewBag.States = _stateService.GetAll().ToList();
                    ViewBag.Departments = _departmentService.GetAll().ToList();
                    ViewBag.Desginations = _designationService.GetAll().ToList();
                    View(model);
                }
                //if (id == null)
                //    return RedirectToAction("Index");
                var emp = Mapper.Map<EmployeeViewModel, Employee>(model);
                _employeeService.Update(emp);
                return RedirectToAction("Index"); 
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var emp = _employeeService.GetById(id);
                _employeeService.Delete(emp);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
