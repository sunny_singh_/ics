﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class VendorController : Controller
    {
        private readonly IVendorService _vendorService;
        private readonly IStateService _stateService;

        public VendorController(IVendorService vendorService, IStateService stateService)
        {
            _vendorService = vendorService;
            _stateService = stateService;
        }

        // GET: Vendor
        public ActionResult Index()
        {
            var vendors = _vendorService.GetAll();
            var vendorsList = Mapper.Map<IEnumerable<Vendor>, IEnumerable<VendorViewModel>>(vendors);
            return View(vendorsList);
        }

        // GET: Vendor/Details/5
        public ActionResult Details(Guid id)
        {
            var vendor = _vendorService.GetById(id);
            var vendorModel = Mapper.Map<Vendor, VendorViewModel>(vendor);
            return View(vendorModel);
        }

        // GET: Vendor/Create
        public ActionResult Create()
        {
            var model = new VendorViewModel();
            var states = _stateService.GetAll();
            var statesViewModel = Mapper.Map<List<StateViewModel>>(states);
            model.States = statesViewModel;
            return View(model);
        }

        // POST: Vendor/Create
        [HttpPost]
        public ActionResult Create(VendorViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var vendor = Mapper.Map<VendorViewModel, Vendor>(model);
                vendor.CreatedTime = DateTime.Now;
                vendor.UserId = User.Identity.GetUserId();
                _vendorService.Insert(vendor);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Vendor/Edit/5
        public ActionResult Edit(Guid id)
        {

            var vendor = _vendorService.GetById(id);

            var vendorModel = Mapper.Map<Vendor, VendorViewModel>(vendor);
            var states = _stateService.GetAll();
            var statesViewModel = Mapper.Map<List<StateViewModel>>(states);
            vendorModel.States = statesViewModel;
            return View(vendorModel);
        }

        // POST: Vendor/Edit/5
        [HttpPost]
        public ActionResult Edit(VendorViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                    return View(model);
                var vendor = Mapper.Map<VendorViewModel, Vendor>(model);
                _vendorService.Update(vendor);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Vendor/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var vendor = _vendorService.GetById(id);
                _vendorService.Delete(vendor);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Vendor/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Vendor model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var vendor = _vendorService.GetById(id);
                _vendorService.Delete(vendor);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
