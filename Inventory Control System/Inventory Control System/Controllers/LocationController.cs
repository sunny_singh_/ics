﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class LocationController : Controller
    {
        private readonly ILocationService _locationService;

        public LocationController(ILocationService locationService)
        {
            _locationService = locationService;
        }
        // GET: Location
        public ActionResult Index()
        {
            var locations = _locationService.GetAll();
            var locationModelList = Mapper.Map<IEnumerable<Location>, IEnumerable<LocationViewModel>>(locations);
            return View(locationModelList);
        }

        // GET: Location/Details/5 
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Location/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Location/Create
        [HttpPost]
        public ActionResult Create(LocationViewModel model)
        {
            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var location = Mapper.Map<LocationViewModel, Location>(model);
                location.UserId = User.Identity.GetUserId();
                location.CreatedTime = DateTime.Now;
                _locationService.Insert(location);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Location/Edit/5
        public ActionResult Edit(Guid id)
        {
            var location = _locationService.GetById(id);
            var locationModel = Mapper.Map<Location, LocationViewModel>(location);
            return View(locationModel);
        }

        // POST: Location/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, LocationViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    View(model);
                }                
                var location = Mapper.Map<LocationViewModel, Location>(model);
                _locationService.Update(location);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Location/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");

                var location = _locationService.GetById(id);
                _locationService.Delete(location);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Location/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
