﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class DesignationController : Controller
    {
        private readonly IDesignationService _designationService;

        public DesignationController(IDesignationService designationService)
        {
            _designationService = designationService;
        }
        // GET: Designation
        public ActionResult Index()
        {
            var desgination = _designationService.GetAll().Select(x => new
             DesignationViewModel
            {
                Id=x.Id,
                Name = x.Name,
                CreatedTime = x.CreatedTime
            }).ToList();
            //var result = Mapper.Map<DesignationViewModel>(desgination);
            return View(desgination);
        }

        // GET: Designation/Details/5
        public ActionResult Details(Guid id)
        {
            return View();
        }

        // GET: Designation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Designation/Create
        [HttpPost]
        public ActionResult Create(DesignationViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View(model);
                var desg = Mapper.Map<DesignationViewModel, Designation>(model);
                desg.UserId = User.Identity.GetUserId();
                desg.CreatedTime = DateTime.Now;
                _designationService.Insert(desg);
                return RedirectToAction("Index");
               
            }
            catch(Exception ex)
            {
                return View(model);
            }
        }

        // GET: Designation/Edit/5
        public ActionResult Edit(Guid id)
        {
            var des = _designationService.GetById(id);
            var desModel = Mapper.Map<Designation, DesignationViewModel>(des);
            return View(desModel);
        }

        // POST: Designation/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, DesignationViewModel model)
        {
            try
            {
                //if (id == null)
                //    return RedirectToAction("Index");
                var des = Mapper.Map<DesignationViewModel, Designation>(model);
                _designationService.Update(des);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Designation/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id==Guid.Empty)
                    return RedirectToAction("Index");
                var des = _designationService.GetById(id);
                _designationService.Delete(des);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Designation/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
