﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class StateController : Controller
    {
        private readonly IStateService _stateService;

        public StateController(IStateService stateService)
        {
            _stateService = stateService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var states = _stateService.GetAll();
            var statesList = Mapper.Map<IEnumerable<State>, IEnumerable<StateViewModel>>(states);
            return View(statesList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var state = _stateService.GetById(id);
            var stateModel = Mapper.Map<State, StateViewModel>(state);
            return View(stateModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            var model = new StateViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(StateViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var state = Mapper.Map<StateViewModel, State>(model);
                state.CreatedTime = DateTime.Now;
                state.UserId = User.Identity.GetUserId();
                _stateService.Insert(state);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {

            var state = _stateService.GetById(id);
            var stateModel = Mapper.Map<State, StateViewModel>(state);
            return View(stateModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(StateViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                    return View(model);
                var state = Mapper.Map<StateViewModel, State>(model);
                _stateService.Update(state);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var state = _stateService.GetById(id);
                _stateService.Delete(state);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, State model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var state = _stateService.GetById(id);
                _stateService.Delete(state);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }
    }
}
