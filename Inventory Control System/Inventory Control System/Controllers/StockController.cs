﻿using AutoMapper;
using InventoryControlSystem.Entity;
using InventoryControlSystem.Service.Contract;
using InventoryControlSystem.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InventoryControlSystem.Controllers
{
    public class StockController : Controller
    {
        private readonly IStockService _stockService;
        private readonly IProductService _productService;

        public StockController(IStockService stockService,IProductService productService)
        {
            _stockService = stockService;
            _productService = productService;
        }

        // GET: Brand
        public ActionResult Index()
        {
            var stocks = _stockService.GetAll();
            var stocksList = Mapper.Map<IEnumerable<Stock>, IEnumerable<StockViewModel>>(stocks);
            return View(stocksList);
        }

        // GET: Brand/Details/5
        public ActionResult Details(Guid id)
        {
            var stock = _stockService.GetById(id);
            var stockModel = Mapper.Map<Stock, StockViewModel>(stock);
            return View(stockModel);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            ViewBag.Allproduct = _productService.GetAll().ToList();
            var model = new StockViewModel();
            return View(model);
        }

        // POST: Brand/Create
        [HttpPost]
        public ActionResult Create(StockViewModel model)
        {

            try
            {
                // TODO: Add insert logic here
                if (!ModelState.IsValid)
                    return View(model);
                var stock = Mapper.Map<StockViewModel, Stock>(model);
                stock.TransactionTime = DateTime.Now;
                stock.UserId = User.Identity.GetUserId();
                _stockService.Insert(stock);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.Allproduct = _productService.GetAll().ToList();
                return View(model);
            }
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(Guid id)
        {
            ViewBag.Allproduct = _productService.GetAll().ToList();
            var stock = _stockService.GetById(id);
            var stockModel = Mapper.Map<Stock, StockViewModel>(stock);
            return View(stockModel);
        }

        // POST: Brand/Edit/5
        [HttpPost]
        public ActionResult Edit(StockViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Allproduct = _productService.GetAll().ToList();
                    return View(model);
                }
                    var stock = Mapper.Map<StockViewModel, Stock>(model);
                _stockService.Update(stock);
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == Guid.Empty)
                    return RedirectToAction("Index");
                var stock = _stockService.GetById(id);
                _stockService.Delete(stock);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // POST: Brand/Delete/5
        [HttpPost]
        public ActionResult Delete(Guid id, Stock model)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index");
                var stock = _stockService.GetById(id);
                _stockService.Delete(stock);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}
