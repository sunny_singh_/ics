﻿using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryControlSystem.DAL.Contract.Repository;
using InventoryControlSystem.DAL.Context;
using System.Data.Entity;

namespace InventoryControlSystem.DAL.Repository
{
   public class DepartmentRepository:GenericRepository<Department,Guid>, IDepartmentRepository
    {
        private readonly ICSContext _dbContext;
        public DepartmentRepository(DbContext dbContext):base(dbContext)
        {
            _dbContext = (_dbContext ?? (ICSContext)dbContext); 
        }

        public IEnumerable<Department> GetDepartment()
        {
            throw new NotImplementedException();
        }
    }
}
