﻿using InventoryControlSystem.DAL.Contract.Repository;
using InventoryControlSystem.DAL.Contract.UnitOfWork;
using InventoryControlSystem.DAL.Repository;
using InventoryControlSystem.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.DAL.UnitOfWork
{
    public class UnitOfWork<TContext> : Disposable, IUnitOfWork<TContext>
        where TContext : DbContext, new()
    {
        public virtual int Commit()
        {
            return _dataContext.SaveChanges();
        }

        public virtual Task<int> CommitAsync()
        {
            return _dataContext.SaveChangesAsync();
        }

        private readonly DbContext _dataContext;

        public UnitOfWork()
        {
            _dataContext = new TContext();
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }

        /// <summary>
        /// Define Repositories
        /// </summary>
        private IDepartmentRepository _departmentRepository;

        public IDepartmentRepository DepartmentRepository
        {
            get { return _departmentRepository ?? (_departmentRepository = new DepartmentRepository(_dataContext)); }
        }

        private IGenericRepository<Brand, Guid> _brandRepository;

        public IGenericRepository<Brand, Guid> BrandRepository
        {
            get { return _brandRepository ?? (_brandRepository = new GenericRepository<Brand, Guid>(_dataContext)); }
        }

        private IGenericRepository<ProductCategory, Guid> _productCategoryRepository;

        public IGenericRepository<ProductCategory, Guid> ProductCategoryRepository
        {
            get { return _productCategoryRepository ?? (_productCategoryRepository = new GenericRepository<ProductCategory, Guid>(_dataContext)); }
        }

        private IGenericRepository<ProductType, Guid> _productTypeRepository;

        public IGenericRepository<ProductType, Guid> ProductTypeRepository
        {
            get { return _productTypeRepository ?? (_productTypeRepository = new GenericRepository<ProductType, Guid>(_dataContext)); }
        }


        private IGenericRepository<Measurement, Guid> _measurementTypeRepository;

        public IGenericRepository<Measurement, Guid> MeasurementRepository
        {
            get { return _measurementTypeRepository ?? (_measurementTypeRepository = new GenericRepository<Measurement, Guid>(_dataContext)); }
        }

        private IGenericRepository<State, Guid> _stateRepository;

        public IGenericRepository<State, Guid> StateRepository
        {
            get { return _stateRepository ?? (_stateRepository = new GenericRepository<State, Guid>(_dataContext)); }
        }

        private IGenericRepository<Stock, Guid> _stockRepository;

        public IGenericRepository<Stock, Guid> StockRepository
        {
            get { return _stockRepository ?? (_stockRepository = new GenericRepository<Stock, Guid>(_dataContext)); }
        }

        //
        private IGenericRepository<Drawer, Guid> _drawerRepository;

        public IGenericRepository<Drawer, Guid> DrawerRepository
        {
            get { return _drawerRepository ?? (_drawerRepository = new GenericRepository<Drawer, Guid>(_dataContext)); }
        }

        private IGenericRepository<Location, Guid> _locationRepository;

        public IGenericRepository<Location, Guid> LocationRepository
        {
            get { return _locationRepository ?? (_locationRepository = new GenericRepository<Location, Guid>(_dataContext)); }
        }

        private IGenericRepository<FinancialYear, Guid> _financialYearRepository;

        public IGenericRepository<FinancialYear, Guid> FinancialYearRepository
        {
            get { return _financialYearRepository ?? (_financialYearRepository = new GenericRepository<FinancialYear, Guid>(_dataContext)); }
        }

        private IGenericRepository<Organisation, Guid> _organisationRepository;

        public IGenericRepository<Organisation, Guid> OrganisationRepository
        {
            get { return _organisationRepository ?? (_organisationRepository = new GenericRepository<Organisation, Guid>(_dataContext)); }
        }

        private IGenericRepository<Employee, Guid> _employeeRepository;

        public IGenericRepository<Employee, Guid> EmployeeRepository
        {
            get { return _employeeRepository ?? (_employeeRepository = new GenericRepository<Employee, Guid>(_dataContext)); }
        }

        private IGenericRepository<Vendor, Guid> _vendorRepository;

        public IGenericRepository<Vendor, Guid> VendorRepository
        {
            get { return _vendorRepository ?? (_vendorRepository = new GenericRepository<Vendor, Guid>(_dataContext)); }
        }

        private IGenericRepository<Product, Guid> _productRepository;

        public IGenericRepository<Product, Guid> ProductRepository
        {
            get { return _productRepository ?? (_productRepository = new GenericRepository<Product, Guid>(_dataContext)); }
        }

        private IGenericRepository<Designation, Guid> _desgnationRepository;

        public IGenericRepository<Designation,Guid> DesignationRepository
        {
            get { return _desgnationRepository ?? (_desgnationRepository = new GenericRepository<Designation, Guid>(_dataContext)); }
        }

       
    }

}
