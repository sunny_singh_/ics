namespace InventoryControlSystem.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brand",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        BrandCode = c.String(nullable: false, maxLength: 20),
                        BrandName = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Createdtime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        DepartmentCode = c.String(nullable: false, maxLength: 20),
                        DepartmentName = c.String(nullable: false),
                        Description = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 50),
                        Name = c.String(nullable: false),
                        Photo = c.String(),
                        DepartmentId = c.Guid(nullable: false),
                        DesignationId = c.Guid(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        MaritalStatus = c.String(maxLength: 3),
                        Nationality = c.String(maxLength: 25),
                        Religion = c.String(maxLength: 20),
                        Qualification = c.String(maxLength: 50),
                        QualificationDetails = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        StateId = c.Guid(nullable: false),
                        PhoneNo = c.String(),
                        MobileNo = c.String(maxLength: 10),
                        Email = c.String(),
                        Pincode = c.String(maxLength: 6),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.Departments", t => t.DepartmentId, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .Index(t => t.DepartmentId)
                .Index(t => t.StateId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Designations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Drawers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        LocationId = c.Guid(nullable: false),
                        DrawerNo = c.String(nullable: false, maxLength: 50),
                        Desription = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        LocationDetails = c.String(nullable: false),
                        Description = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Code = c.String(),
                        Name = c.String(),
                        ModelNo = c.String(),
                        Version = c.String(),
                        MinLevel = c.String(),
                        MaxLevel = c.String(),
                        Description = c.String(),
                        MeasurementId = c.Guid(nullable: false),
                        CreatedTime = c.DateTime(nullable: false),
                        UserID = c.String(maxLength: 128),
                        ProductTypesId = c.Guid(nullable: false),
                        CategoryId = c.Guid(nullable: false),
                        BrandId = c.Guid(nullable: false),
                        DrawerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .ForeignKey("dbo.Brand", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.Drawers", t => t.DrawerId, cascadeDelete: true)
                .ForeignKey("dbo.ProductCategories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.ProductTypes", t => t.ProductTypesId, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.ProductTypesId)
                .Index(t => t.CategoryId)
                .Index(t => t.BrandId)
                .Index(t => t.DrawerId);
            
            CreateTable(
                "dbo.ProductCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        CatCode = c.String(nullable: false, maxLength: 20),
                        PCategory = c.String(nullable: false),
                        Description = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ProductTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TypeCode = c.String(nullable: false, maxLength: 20),
                        PType = c.String(nullable: false),
                        Description = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        PurchaseRate = c.Decimal(precision: 18, scale: 2),
                        MRP = c.Decimal(precision: 18, scale: 2),
                        TransactionNote = c.String(maxLength: 100),
                        TransactionTime = c.DateTime(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.FinancialYears",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FromYear = c.DateTime(nullable: false),
                        ToYear = c.DateTime(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Measurements",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ShortCode = c.String(nullable: false, maxLength: 20),
                        MeasurementName = c.String(nullable: false, maxLength: 50),
                        ConversionValue = c.String(maxLength: 5),
                        EquivalentMeasurement = c.String(maxLength: 50),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Organisations",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        OrgName = c.String(nullable: false, maxLength: 100),
                        CSTNo = c.String(maxLength: 25),
                        RegistrationNo = c.String(maxLength: 25),
                        ContactPerson = c.String(),
                        Address = c.String(),
                        StateId = c.Guid(nullable: false),
                        City = c.String(),
                        MobileNo = c.String(maxLength: 10),
                        PhoneNo = c.String(maxLength: 25),
                        PINno = c.String(maxLength: 6),
                        EmailId = c.String(),
                        Website = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        VendorCode = c.String(nullable: false, maxLength: 20),
                        VendorName = c.String(nullable: false),
                        DealerCode = c.String(nullable: false, maxLength: 15),
                        CSTNo = c.String(maxLength: 15),
                        ContactPerson = c.String(nullable: false),
                        Address = c.String(),
                        StateId = c.Guid(nullable: false),
                        Pin = c.String(maxLength: 8),
                        Phone = c.String(maxLength: 20),
                        Mobile = c.String(maxLength: 10),
                        Email = c.String(),
                        Description = c.String(),
                        CreatedTime = c.DateTime(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.UserLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Ipdetails = c.String(nullable: false, maxLength: 15),
                        LoginTime = c.DateTime(nullable: false),
                        LogOutTime = c.DateTime(nullable: false),
                        BrowserDetails = c.String(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserLogs", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Brand", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Vendors", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Organisations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Measurements", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FinancialYears", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Stocks", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Stocks", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "ProductTypesId", "dbo.ProductTypes");
            DropForeignKey("dbo.ProductTypes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.ProductCategories");
            DropForeignKey("dbo.ProductCategories", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "DrawerId", "dbo.Drawers");
            DropForeignKey("dbo.Products", "BrandId", "dbo.Brand");
            DropForeignKey("dbo.Products", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Drawers", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Drawers", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Designations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Employees", "StateId", "dbo.States");
            DropForeignKey("dbo.States", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Employees", "DepartmentId", "dbo.Departments");
            DropForeignKey("dbo.Employees", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Departments", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserLogs", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Vendors", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.Organisations", new[] { "UserId" });
            DropIndex("dbo.Measurements", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.FinancialYears", new[] { "UserId" });
            DropIndex("dbo.Stocks", new[] { "UserId" });
            DropIndex("dbo.Stocks", new[] { "ProductId" });
            DropIndex("dbo.ProductTypes", new[] { "UserId" });
            DropIndex("dbo.ProductCategories", new[] { "UserId" });
            DropIndex("dbo.Products", new[] { "DrawerId" });
            DropIndex("dbo.Products", new[] { "BrandId" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.Products", new[] { "ProductTypesId" });
            DropIndex("dbo.Products", new[] { "UserID" });
            DropIndex("dbo.Locations", new[] { "UserId" });
            DropIndex("dbo.Drawers", new[] { "UserId" });
            DropIndex("dbo.Drawers", new[] { "LocationId" });
            DropIndex("dbo.Designations", new[] { "UserId" });
            DropIndex("dbo.States", new[] { "UserId" });
            DropIndex("dbo.Employees", new[] { "UserId" });
            DropIndex("dbo.Employees", new[] { "StateId" });
            DropIndex("dbo.Employees", new[] { "DepartmentId" });
            DropIndex("dbo.Departments", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Brand", new[] { "UserId" });
            DropTable("dbo.UserLogs");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Vendors");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Organisations");
            DropTable("dbo.Measurements");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.FinancialYears");
            DropTable("dbo.Stocks");
            DropTable("dbo.ProductTypes");
            DropTable("dbo.ProductCategories");
            DropTable("dbo.Products");
            DropTable("dbo.Locations");
            DropTable("dbo.Drawers");
            DropTable("dbo.Designations");
            DropTable("dbo.States");
            DropTable("dbo.Employees");
            DropTable("dbo.Departments");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Brand");
        }
    }
}
