﻿using InventoryControlSystem.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.DAL.Context
{
    public class ICSContext : IdentityDbContext<ApplicationUser>
    {
        public ICSContext()
            : base("ICS", throwIfV1Schema: false)
        {
        }
        DbSet<Brand> Brands { get;set;}
        DbSet<Department> Departments { get; set; }
        DbSet<Drawer> Drawers { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<FinancialYear> FinancialYears { get; set; }
        DbSet<Location> Locations { get; set; }
        DbSet<Measurement> Measurements { get; set; }
        DbSet<Organisation> Organisations { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<ProductCategory> ProductCategorys { get; set; }
        DbSet<ProductType> ProductTypes { get; set; }
        DbSet<State> States { get; set;}
        DbSet<UserLog> UserLogs { get; set; }
        DbSet<Vendor> Vender { get; set; }

        DbSet<Designation> Designations { get; set; }
        public static ICSContext Create()
        {
            return new ICSContext();
        }
    }
}
