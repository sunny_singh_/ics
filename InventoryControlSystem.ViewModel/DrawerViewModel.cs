﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
   public class DrawerViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public Guid LocationId { get; set; }
        [Required]
        public string DrawerNo { get; set; }
        [Required]
        public string Desription { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }
    }
}
