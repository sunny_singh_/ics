﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class ProductViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ModelNo { get; set; }
        [Required]
        public string Version { get; set; }
        [Required]
        public string MinLevel { get; set; }
        [Required]
        public string MaxLevel { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Guid MeasurementId { get; set; }
        public string Measurement { get; set; }
        public List<MeasurementViewModel> Measurements { get; set; }
        public DateTime CreatedTime { get; set; }
        [Required]
        [DisplayName("Type")]
        public Guid ProductTypesId { get; set; }
        public string ProductType { get; set; }
        public List<ProductTypeViewModel> ProductTypes { get; set; }
        [Required]
        [DisplayName("Category")]
        public Guid CategoryId { get; set; }
        public string Category { get; set; }
        public List<ProductCategoryViewModel> ProductCatgories { get; set; }
        [Required]
        [DisplayName("Brand")]
        public Guid BrandId { get; set; }
        public string Brand { get; set; }
        public List<BrandViewModel> Brands { get; set; }
        [Required]
        [DisplayName("Drawer")]
        public Guid DrawerId { get; set; }
        public string Drawer { get; set; }
        public List<DrawerViewModel> Drawers { get; set; }
        [Required]
        [DisplayName("Stock")]
        public Guid StockId { get; set; }
        public int Stock { get; set; }
        public List<StockViewModel> Stocks { get; set; }

        public string UserName { get; set; }
    }
}
