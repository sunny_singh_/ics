﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class LocationViewModel
    {
        public Guid Id { get; set; }
        [DisplayName("Location")]
        [Required]
        public string LocationDetails { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }
    }
}
