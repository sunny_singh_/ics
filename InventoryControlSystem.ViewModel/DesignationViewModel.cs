﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
  public  class DesignationViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }

        public DateTime CreatedTime { get; set; }

        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public string UserName { get; set; }

    }
}
