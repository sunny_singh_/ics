﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
   public class FinancialYearViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public DateTime FromYear { get; set; }
        [Required]
        public DateTime ToYear { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UserName { get; set; }

    }
}
