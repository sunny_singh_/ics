﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class BrandViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string BrandCode { get; set; }
        [Required]
        public string BrandName { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime Createdtime { get; set; }
        public string UserName { get; set; }

    }
}
