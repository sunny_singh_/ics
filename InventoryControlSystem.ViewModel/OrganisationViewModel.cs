﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
   public class OrganisationViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string OrgName { get; set; }
        [Required]
        public string CSTNo { get; set; }
        [Required]
        public string RegistrationNo { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public Guid StateId { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string MobileNo { get; set; }
        [Required]
        public string PhoneNo { get; set; }
        [Required]
        public string PINno { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Website { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }

    }
}
