﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class StockViewModel
    {
        public Guid Id { get; set; }
        
        public decimal? PurchaseRate { get; set; }

        public decimal? MRP { get; set; }
        [Required]
        public string TransactionNote { get; set; }
        [Required]
        public DateTime TransactionTime { get; set; }
        public Guid ProductId { get; set; }
        public string UserName { get; set; }
    }
}
