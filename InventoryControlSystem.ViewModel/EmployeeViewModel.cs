﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
   public class EmployeeViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Photo { get; set; }
        [Required]
        public Guid DepartmentId { get; set; }
        [Required]
        public Guid DesignationId { get; set; }
        [Required]
        public DateTime DOB { get; set; }
        [Required]
        public string MaritalStatus { get; set; }
        [Required]
        public string Nationality { get; set; }
        [Required]
        public string Religion { get; set; }
        [Required]
        public string Qualification { get; set; }
        [Required]
        public string QualificationDetails { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public Guid StateId { get; set; }
        [Required]
        public string PhoneNo { get; set; }
        [Required]
        public string MobileNo { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Pincode { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }
    }
}
