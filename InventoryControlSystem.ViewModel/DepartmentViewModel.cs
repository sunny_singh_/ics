﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class DepartmentViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string DepartmentCode { get; set; }
        [Required]
        public string DepartmentName { get; set; }
        [Required]
        public string Description { get; set; }

        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }
    }
}
