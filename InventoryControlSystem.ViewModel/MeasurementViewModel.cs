﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class MeasurementViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string ShortCode { get; set; }
        [Required]
        public string MeasurementName { get; set; }
        [Required]
        public string ConversionValue { get; set; }
        [Required]
        public string EquivalentMeasurement { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }

    }
}
