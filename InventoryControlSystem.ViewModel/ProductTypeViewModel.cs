﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class ProductTypeViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string TypeCode { get; set; }
        [Required]
        public string PType { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }

    }
}
