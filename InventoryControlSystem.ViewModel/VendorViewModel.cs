﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.ViewModel
{
    public class VendorViewModel
    {
        public Guid Id { get; set; }
        [Required]
        public string VendorCode { get; set; }
        [Required]
        public string VendorName { get; set; }
        [Required]
        public string DealerCode { get; set; }
        [Required]
        public string CSTNo { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public Guid StateId { get; set; }
        public List<StateViewModel> States { get; set; }
        [Required]
        public string Pin { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Mobile { get; set; }
        public string Email { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserName { get; set; }

    }
}
