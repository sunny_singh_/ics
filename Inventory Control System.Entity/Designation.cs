﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
   public class Designation : BaseEntity<Guid>
    {
       
        public string Name { get; set; }

        public DateTime CreatedTime { get; set; }


        [ForeignKey("ApplicationUsers")]
        public string UserId { get; set; }

        public virtual ApplicationUser ApplicationUsers { get; set; }

        public bool IsDeleted { get; set; }

    }
}
