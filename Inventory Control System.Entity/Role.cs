﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
  public  class Role:BaseEntity<Guid>
    {
        [MaxLength]
        [Required]
        public string Name { get; set; }
        [MaxLength]
        public string Remarks { get; set; }
        public DateTime CreatedTime { get; set; }
        public string UserId { get; set; }
    }
}
