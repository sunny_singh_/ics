﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
   public class Location:BaseEntity<Guid>
    {
        [MaxLength]
        [Required]
        public string LocationDetails { get; set; }
        [MaxLength]
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Drawer> Drawers { get; set; }
    }
}
