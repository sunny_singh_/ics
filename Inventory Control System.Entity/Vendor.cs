﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
    public class Vendor : BaseEntity<Guid>
    {

        [MaxLength(20)]
        [StringLength(20)]
        [Required]
        public string VendorCode { get; set; }
        [MaxLength]
        [Required]
        public string VendorName { get; set; }
        [MaxLength(15)]
        [StringLength(15)]
        [Required]
        public string DealerCode { get; set; }
        [MaxLength(15)]
        [StringLength(15)]
        public string CSTNo { get; set; }
        [MaxLength]
        [Required]
        public string ContactPerson { get; set; }
        [MaxLength]
        public string Address { get; set; }
        public Guid StateId { get; set; }
        [MaxLength(8)]
        [StringLength(8)]
        public string Pin { get; set; }
        [MaxLength(20)]
        [StringLength(20)]
        public string Phone { get; set; }
        [MaxLength(10)]
        [StringLength(10)]
        public string Mobile { get; set; }
        [MaxLength]
        public string Email { get; set; }
        [MaxLength]
        public string Description { get; set; }

        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
