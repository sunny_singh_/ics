﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
   public class Employee:BaseEntity<Guid>
    {
        [MaxLength(50)]
        [StringLength(50)]
        [Required]
        public string Code { get; set; }
        [MaxLength]
        [Required]
        public string Name { get; set; }
        [MaxLength]
        public string Photo { get; set; }
        [Required]

        [ForeignKey("Departments")]
        public Guid DepartmentId { get; set; }
        [Required]
        public Guid DesignationId { get; set; }
        [Required]
        public DateTime DOB { get; set; }
        [MaxLength(3)]
        [StringLength(3)]
        public string MaritalStatus { get; set; }
        [MaxLength(25)]
        [StringLength(25)]
        public string Nationality { get; set; }
        [MaxLength(20)]
        [StringLength(20)]
        public string Religion { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        public string Qualification { get; set; }
        [MaxLength]
        public string QualificationDetails { get; set; }
        [MaxLength]
        public string Address { get; set; }
        [MaxLength]
        public string City { get; set; }

        [ForeignKey("States")]
        public Guid StateId { get; set; }
        [MaxLength]
        public string PhoneNo { get; set; }
        [MaxLength(10)]
        [StringLength(10)]
        public string MobileNo { get; set; }
        [MaxLength]
        public string Email { get; set; }
        [MaxLength(6)]
        [StringLength(6)]
        public string Pincode { get; set; }
        public DateTime CreatedTime { get; set; }
        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual State States { get; set; }

        public virtual Department Departments { get; set; }
    }
}
