﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
    public class Drawer : BaseEntity<Guid>
    {
        [ForeignKey("Location")]
        public Guid LocationId { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        [Required]
        public string DrawerNo { get; set; }
        [MaxLength]
        public string Desription { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public virtual Location Location { get; set; }
    }
}
