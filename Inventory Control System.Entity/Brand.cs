﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
    
  [Table("Brand")]
public class Brand : BaseEntity<Guid>
    {
        
        [MaxLength(20)]
        [StringLength(20)]
        [Required]
        public string BrandCode { get; set; }
        [MaxLength]
        [Required]
        public string BrandName { get; set; }
        [MaxLength]
        [Required]
        public string Description { get; set; }
        public DateTime Createdtime { get; set; }

        [ForeignKey("ApplicationUsers")]
        public string UserId { get; set; } 

        public virtual ApplicationUser ApplicationUsers { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
