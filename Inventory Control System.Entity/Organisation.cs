﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
  public  class Organisation:BaseEntity<Guid>
    {
        [MaxLength(100)]
        [StringLength(100)]
        [Required]
        public string OrgName { get; set; }
        [MaxLength(25)]
        [StringLength(25)]
        public string CSTNo { get; set; }
        [MaxLength(25)]
        [StringLength(25)]
        public string RegistrationNo { get; set; }
        [MaxLength]
        public string ContactPerson { get; set; }
        [MaxLength]
        public string Address { get; set; }
        public Guid StateId { get; set; }
        [MaxLength]
        public string City { get; set; }
        [MaxLength(10)]
        [StringLength(10)]
        public string MobileNo { get; set; }
        [MaxLength(25)]
        [StringLength(25)]
        public string PhoneNo { get; set; }
        [MaxLength(6)]
        [StringLength(6)]
        public string PINno { get; set; }
        [MaxLength]
        public string EmailId { get; set; }
        [MaxLength]
        public string Website { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
