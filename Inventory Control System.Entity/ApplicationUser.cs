﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public virtual ICollection<Brand> Brands { get; set; }

        public virtual ICollection<Department> Departments { get; set; }

        public virtual ICollection<Drawer> Drawers { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public virtual ICollection<FinancialYear> FinancialYears { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<Measurement> Measurements { get; set; }

        public virtual ICollection<Organisation> Organisations { get; set; }


        public virtual ICollection<Product> Products { get; set; }

        public virtual ICollection<ProductCategory> ProductCategories { get; set; }

        public virtual ICollection<ProductType> ProductTypes { get; set; }

        public virtual ICollection<State> States { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }

        public virtual ICollection<Vendor> Vendors { get; set; }

        public virtual ICollection<Designation> Designations { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
