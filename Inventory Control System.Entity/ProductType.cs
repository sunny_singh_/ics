﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
  public  class ProductType:BaseEntity<Guid>
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required]
        public string TypeCode { get; set; }
        [MaxLength]
        [Required]
        public string PType { get; set; }
        [MaxLength]
        public string Description { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
