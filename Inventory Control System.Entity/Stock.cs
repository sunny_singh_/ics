﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
    public class Stock : BaseEntity<Guid>
    {

        public decimal? PurchaseRate { get; set; }
        public decimal? MRP { get; set; }
        [MaxLength(100)]
        [StringLength(100)]
        public string TransactionNote { get; set; }
        public DateTime TransactionTime { get; set; }

        [ForeignKey("Product")]
        public Guid ProductId { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual Product Product { get; set; }
    }
}
