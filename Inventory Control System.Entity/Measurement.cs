﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
    public class Measurement : BaseEntity<Guid>
    {
        [MaxLength(20)]
        [StringLength(20)]
        [Required]
        public string ShortCode { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        [Required]
        public string MeasurementName { get; set; }
        [MaxLength(5)]
        [StringLength(5)]
        public string ConversionValue { get; set; }
        [MaxLength(50)]
        [StringLength(50)]
        public string EquivalentMeasurement { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
