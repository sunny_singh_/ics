﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryControlSystem.Entity
{
   public class Product:BaseEntity<Guid>
    {
        public string Code { get; set; }
        public string Name { get; set; }
       
        public string ModelNo { get; set; }
        public string Version { get; set; }
      
        public string MinLevel { get; set; }
        public string MaxLevel { get; set; }
        public string Description { get; set; }
        public Guid MeasurementId { get; set; }
        public DateTime CreatedTime { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserID { get; set; }

        [ForeignKey("ProductType")]
        public Guid ProductTypesId { get; set; }

        [ForeignKey("ProductCatgory")]
        public Guid CategoryId { get; set; }

        [ForeignKey("Brand")]
        public Guid BrandId { get; set; }

        [ForeignKey("Drawer")]
        public Guid DrawerId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Stock> Stocks { get; set; }

        public virtual ProductType ProductType { get; set; }

        public virtual ProductCategory ProductCatgory { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Drawer Drawer { get; set; }
    }
}


